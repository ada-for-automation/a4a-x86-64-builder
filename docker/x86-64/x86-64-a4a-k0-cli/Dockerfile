
# Ada for Automation Registry
ARG A4A_REGISTRY=registry.gitlab.com/ada-for-automation/docker

# Basis image
ARG A4A_BASIS_IMG=a4a-x86-64-builder/x86-64-a4a-basis-dev:latest

# Use latest Ada for Automation Basis Development as build image
FROM $A4A_REGISTRY/$A4A_BASIS_IMG AS build

# Use latest debian as base image
FROM debian:bookworm AS base

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian (bookworm) / A4A-K0-CLI"

# Version
ENV A4A_K0_CLI=0.0.1

# Install required libraries
RUN apt-get update  \
    && apt-get install -y \
    libgnat-12 libmodbus5

# Application path
ARG LOCATION="/home/pi/Ada/A4A/demo/000 a4a-k0-cli"

# Install Ada for Automation demo application
COPY --from=build \
    $LOCATION/bin/a4a_k0_cli_demo $LOCATION/bin/a4a_k0_cli_demo

WORKDIR $LOCATION

# The entrypoint shall start the demo application
ENTRYPOINT ["./bin/a4a_k0_cli_demo", "--log-level=info", "--log-color=no"]

# Modbus TCP Server port
EXPOSE 1504

# Set STOPSIGNAL
STOPSIGNAL SIGINT


